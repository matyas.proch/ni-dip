\changetocdepth {3}
\babel@toc {czech}{}
\babel@toc {czech}{}
\babel@toc {english}{}
\babel@toc {czech}{}
\babel@toc {english}{}
\babel@toc {czech}{}
\contentsline {chapter}{{\' U}vod}{1}{chapter*.8}%
\contentsline {chapter}{\chapternumberline {1}Cíl práce}{3}{chapter.1}%
\contentsline {chapter}{\chapternumberline {2}Analýza požadavků}{5}{chapter.2}%
\contentsline {section}{\numberline {2.1}Existující řešení}{5}{section.2.1}%
\contentsline {subsection}{\numberline {2.1.1}Mobilní aplikace}{5}{subsection.2.1.1}%
\contentsline {subsubsection}{\numberline {2.1.1.1}WalkSafe}{5}{subsubsection.2.1.1.1}%
\contentsline {subsubsection}{\numberline {2.1.1.2}Personal Safety}{6}{subsubsection.2.1.1.2}%
\contentsline {subsubsection}{\numberline {2.1.1.3}One Scream - Personal Safety}{6}{subsubsection.2.1.1.3}%
\contentsline {subsubsection}{\numberline {2.1.1.4}Další mobilní aplikace}{6}{subsubsection.2.1.1.4}%
\contentsline {subsection}{\numberline {2.1.2}Volání blízkým a infolinky}{6}{subsection.2.1.2}%
\contentsline {subsection}{\numberline {2.1.3}Shrnutí}{6}{subsection.2.1.3}%
\contentsline {section}{\numberline {2.2}Funkční požadavky}{7}{section.2.2}%
\contentsline {subsection}{\numberline {2.2.1}Profil uživatele}{7}{subsection.2.2.1}%
\contentsline {subsection}{\numberline {2.2.2}Nouzové kontakty}{7}{subsection.2.2.2}%
\contentsline {subsection}{\numberline {2.2.3}Oblíbené destinace}{7}{subsection.2.2.3}%
\contentsline {subsection}{\numberline {2.2.4}Sledování polohy}{7}{subsection.2.2.4}%
\contentsline {subsection}{\numberline {2.2.5}Módy interakce}{7}{subsection.2.2.5}%
\contentsline {subsubsection}{\numberline {2.2.5.1}Pravidelné reagování}{7}{subsubsection.2.2.5.1}%
\contentsline {subsubsection}{\numberline {2.2.5.2}Simulace hovoru}{7}{subsubsection.2.2.5.2}%
\contentsline {subsection}{\numberline {2.2.6}Stav baterie}{7}{subsection.2.2.6}%
\contentsline {subsection}{\numberline {2.2.7}Bezpečnost proti kompromitaci útočníkem}{8}{subsection.2.2.7}%
\contentsline {section}{\numberline {2.3}Nefunkční požadavky}{8}{section.2.3}%
\contentsline {chapter}{Z{\' a}v{\v e}r}{9}{chapter*.9}%
\contentsline {chapter}{Literatura}{11}{chapter*.10}%
