**Vysvětlivky**

- ✅ - Hotovo
- 🚼 - Rozpracováno
- 🆘 - Nezačato
- (datum) - Předpokládaný datum dokončení

**Úkoly mimo harmonogram**

- 🚼 Klíčová slova
- 🆘 Abstrakt
- 🆘 Úvod
- 🆘 Závěr
- 🆘 Kontrola citací
- 🆘 Kontrola formalit
- 🆘 Odevzdávní práce

**Harmonogram**

- 🚼 (13.02.2022) Řešerše stávajících řešení
- 🚼 (13.02.2022) Navržená funkcionalita
- 🆘 (20.02.2022) Návrh aplikace
  - 🆘 (20.02.2022) Design
  - 🆘 (20.02.2022) Průzkum technologií
  - 🆘 (20.02.2022) Průzkum SMS bran
  - 🆘 (20.02.2022) Návrh architektury
  - 🆘 (20.02.2022) Diagram tříd
- 🆘 (06.03.2022) Implementace
  - 🆘 (27.02.2022) Serverová část
  - 🆘 (06.03.2022) iOS Aplikace
- 🆘 (13.03.2022) Nasazení na App Store
- 🆘 (27.03.2022) Testování
  - 🆘 (27.03.2022) Uživatelské testování v reálném světě
- 🆘 (03.04.2022) Zhodnocení výsledků
